//
//  UIScrollView+AutoContentSize.h
//  jinke4iphone
//
//  Created by JK210 on 2018/4/4.
//  Copyright © 2018年 Feng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (AutoContentSize)

@property (nonatomic,assign) CGFloat space;

/** 获取计算出的contentHeigh */
@property (nonatomic,assign) CGFloat contentHeigh;

/** 控件和约束添加完毕,调用此方法 参数space代表最底部控件距离scrollView的底部间隔 */
- (void)setupContentSizeWithBottomSpace:(CGFloat)space;

@end
