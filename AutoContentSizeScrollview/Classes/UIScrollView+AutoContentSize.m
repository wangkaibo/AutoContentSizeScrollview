//
//  UIScrollView+AutoContentSize.m
//  jinke4iphone
//
//  Created by JK210 on 2018/4/4.
//  Copyright © 2018年 Feng. All rights reserved.
//

#import "UIScrollView+AutoContentSize.h"
#import <objc/runtime.h>

static const void *kSpace = "space";
static const void *kContentHeigh = "contentHeigh";

@implementation UIScrollView (AutoContentSize)

@dynamic contentHeigh;

- (void)setContentHeigh:(CGFloat)contentHeigh
{
    objc_setAssociatedObject(self, kContentHeigh, [NSNumber numberWithFloat:contentHeigh], OBJC_ASSOCIATION_ASSIGN);
}

- (CGFloat)contentHeigh
{
    [self layoutIfNeeded];
    CGFloat contentHeigh = 0.00;
    for (id obj in self.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView * view = (UIView *)obj;
            CGRect rect = view.frame;
            if (rect.origin.y+rect.size.height > contentHeigh) {
                contentHeigh = rect.origin.y+rect.size.height;
                self.contentSize = CGSizeMake(self.bounds.size.width, contentHeigh + self.space);
            }
        }
    }
    return contentHeigh;
}

@dynamic space;
- (CGFloat)space
{
    return [objc_getAssociatedObject(self, kSpace) floatValue];
}

- (void)setSpace:(CGFloat)space
{
    objc_setAssociatedObject(self, kSpace, [NSNumber numberWithFloat:space], OBJC_ASSOCIATION_ASSIGN);
}

- (void)setupContentSizeWithBottomSpace:(CGFloat)space
{
    [self layoutIfNeeded];
    self.space = space;
    [self resetContentSizeWithBottomSpace:space];
    [self addObserverToSubviews];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
     NSLog(@"================11111111");
    if ([keyPath isEqualToString:@"bounds"]) {
        [self setupContentSizeWithBottomSpace:self.space];
    }
    NSLog(@"================2222222");
}

//更新scrollview的contentSize
- (void)resetContentSizeWithBottomSpace:(CGFloat)space
{
    CGFloat contentHeigh = 0.00;
    for (id obj in self.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView * view = (UIView *)obj;
            CGRect rect = view.frame;
            if (rect.origin.y+rect.size.height > contentHeigh) {
                contentHeigh = rect.origin.y+rect.size.height;
                self.contentSize = CGSizeMake(self.bounds.size.width, contentHeigh + space);
            }
        }
    }
}

//子视图添加观察者
- (void)addObserverToSubviews
{
    for (id obj in self.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView * view = (UIView *)obj;
            //防止多次添加监听
            @try {
                [view removeObserver:self forKeyPath:@"bounds"];
            }
            @catch (NSException *exception) {
                //当前没有监听,防止崩溃
            }
            
            [view addObserver:self forKeyPath:@"bounds" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
        }
    }
}

- (void)dealloc
{
    for (id obj in self.subviews) {
        if ([obj isKindOfClass:[UIView class]]) {
            UIView * view = (UIView *)obj;
            @try {
                [view removeObserver:self forKeyPath:@"bounds"];
            }
            @catch (NSException *exception) {
                //当前没有监听,防止崩溃
            }
        }
    }
}

@end
